﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day2_Puzzle1
{
    class d2_P1
    {
		internal static int R, C;
		internal static int[] x = new int[] { 1, 0 };
		internal static int[] y = new int[] { 0, 1 };
		internal static bool wordfind(char[][] grid, int row, int col, string word)
		{
			if (grid[row][col] != word[0])
			{
				return false;
			}
			int len = word.Length;
			for (int dir = 0; dir < 2; dir++)
			{
				int k, rd = row + x[dir], cd = col + y[dir];
				for (k = 1; k < len; k++)
				{
					if (rd >= R || rd < 0 || cd >= C || cd < 0)
					{
						break;
					}
					if (grid[rd][cd] != word[k])
					{
						break;
					}
					rd += x[dir];
					cd += y[dir];
				}
				if (k == len)
				{
					return true;
				}
			}
			return false;
		}
		internal static void patternSearch(char[][] grid, string word)
		{
			for (int row = 0; row < R; row++)
			{
				for (int col = 0; col < C; col++)
				{
					if (wordfind(grid, row, col, word))
					{
						Console.Write("TRUE pattern " + word + " found ");
					
					}
				}
			}
		}
		public static void Main(string[] args)
		{
			R = 4;
			C = 4;
			char[][] grid = new char[][]
			{
                new char[] {'F', 'A', 'C', 'I'},
				new char[] {'O', 'B', 'Q', 'P'},
                new char[] {'A', 'N', 'O', 'B'},
				new char[] {'M', 'A', 'S', 'S'}
			};
			patternSearch(grid, "FOAM");
			Console.WriteLine();
			patternSearch(grid, "MASS");
			Console.WriteLine();
		}
	}








}

