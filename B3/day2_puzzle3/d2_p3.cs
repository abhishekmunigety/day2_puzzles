﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day2_puzzle3
{
    class d2_p3
    {
			static int Largestmultiple(int[] arr, int n)
			{
				if (n < 3)
					return -1;

				int largenum = int.MinValue;

				for (int i = 0; i < n - 2; i++)
					for (int j = i + 1; j < n - 1; j++)
						for (int k = j + 1; k < n; k++)
							largenum = Math.Max(largenum,
									arr[i] * arr[j] * arr[k]);

				return largenum;
			}

			// Driver Code
			public static void Main()
			{
			//int[] arr = { -10, -10, 5, 2 };
			int[] arr = new int[5];
			int i;
			Console.WriteLine("enter 5 elements in array to find maximum product of 3 numbers");
			for(i=0;i<5;i++)
            {
				Console.WriteLine("element "+"" + i);
				arr[i] = Convert.ToInt32(Console.ReadLine());
            }

				int n = arr.Length; ;

				int max = Largestmultiple(arr, n);

				if (max == -1)
					Console.WriteLine("No such product exists");
				else
					Console.WriteLine("the  product of 3 larger numbers is " + max);
			}
		}

	}
