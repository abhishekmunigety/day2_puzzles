﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day2_puzzle2
{
    class d2_p2
    {
		static bool multiset(int[] arr, int n)
		{
            int sum = 0;
			int i, j;
			for (i = 0; i < n; i++)
				sum += arr[i];
                if (sum % 2 != 0)
				return false;

			   bool[,] part = new bool[sum / 2 + 1, n + 1];

			for (i = 0; i <= n; i++)
				part[0, i] = true;

			for (i = 1; i <= sum / 2; i++)
				part[i, 0] = false;

			for (i = 1; i <= sum / 2; i++)
			{
				for (j = 1; j <= n; j++)
				{
					part[i, j] = part[i, j - 1];
					if (i >= arr[j - 1])
						part[i, j]= part[i, j - 1] || part[i - arr[j - 1], j - 1];
				}
			}

			return part[sum/2, n];
		}
        public static void Main(string[] args)
		{
			int[] arr = new int[5];
			int i;
			
			Console.Write("Input 5 elements to see if they can be devided into 2 equal sum:\n");
			for (i = 0; i < 5; i++)
			{
				Console.WriteLine("integer element "+""+i);
				arr[i] = Convert.ToInt32(Console.ReadLine());
			}

			int n = arr.Length;

			if (multiset(arr, n) == true)
				Console.WriteLine(" True " + " Can be divided into two subsets of equal sum");
			else
				Console.WriteLine("False " +  "Cannot divided into two subsets of equal sum");
		}
	}
}
