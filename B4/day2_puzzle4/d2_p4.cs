﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day2_puzzle4
{
	class d2_p4
	{
		public static int n_num(int n)
		{
			int count = 0;
			for (int cur = 19; ; cur += 9)
			{
				int sum = 0;
				for (int x = cur;
					x > 0; x = x / 10)
					sum = sum + x % 10;
				if (sum == 10)
					count++;
				if (count == n)
					return cur;
			}
		}

		// Driver Code
		static public void Main(string[] args)
		{
			Console.WriteLine("nth value of 1 is" +n_num(1));
			Console.WriteLine("nth value os 2 is " +n_num(2));

		}
	}
}
	

