﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day2_puzzle5
{
    class d2_p5
    {
		static int maxProfit(int[] price, int start, int end)
		{
			if (end <= start)
				return 0;

			int profit = 0;
			for (int i = start; i < end; i++)
			{
				for (int j = i + 1; j <= end; j++)
				{
					if (price[j] > price[i])
					{

						int curr_profit = price[j] - price[i]
										+ maxProfit(price, start, i - 1)
										+ maxProfit(price, j + 1, end);

						profit = Math.Max(profit, curr_profit);
					}
				}
			}
			return profit;
		}
		public static void Main(String[] args)
		{
			int[] price = new int[6];
		
			for(int i=0;i<6;i++)
            {
				Console.WriteLine("the elemetnts are " + "" +i);
				price[i] = Convert.ToInt32(Console.ReadLine());
            }
		
			int n = price.Length;

			Console.WriteLine("the maximum profit is " + maxProfit(price, 1, n - 1));
		}
	}

}
